package cl.mauledev.hackernews.di.components

import cl.mauledev.hackernews.di.modules.TopicModule
import cl.mauledev.hackernews.di.scope.ActivityScope
import cl.mauledev.hackernews.view.viewmodels.MainViewModel
import dagger.Subcomponent

@ActivityScope
@Subcomponent(modules = [(TopicModule::class)])
interface TopicComponent {

    fun inject(viewModel: MainViewModel)

}