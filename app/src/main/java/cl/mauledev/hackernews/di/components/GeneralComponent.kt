package cl.mauledev.hackernews.di.components

import android.app.Application
import cl.mauledev.hackernews.di.modules.GeneralModule
import cl.mauledev.hackernews.di.modules.TopicModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(GeneralModule::class)])
interface GeneralComponent {

    fun inject(application: Application)

    fun plus(module: TopicModule): TopicComponent
}