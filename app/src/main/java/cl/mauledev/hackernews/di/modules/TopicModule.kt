package cl.mauledev.hackernews.di.modules

import cl.mauledev.hackernews.data.datasources.local.AppDatabase
import cl.mauledev.hackernews.data.datasources.local.LocalDataSource
import cl.mauledev.hackernews.data.datasources.remote.RemoteDataSource
import cl.mauledev.hackernews.data.datasources.remote.api.NewsAPI
import cl.mauledev.hackernews.data.repositories.TopicRepository
import cl.mauledev.hackernews.view.viewmodels.MainViewModel
import dagger.Module
import dagger.Provides

@Module
class TopicModule(val viewModel: MainViewModel) {

    @Provides
    fun providesLocalDataSource(appDatabase: AppDatabase): LocalDataSource {
        return LocalDataSource(appDatabase)
    }

    @Provides
    fun providesRemoteDataSource(newsAPI: NewsAPI): RemoteDataSource {
        return RemoteDataSource(newsAPI)
    }

    @Provides
    fun providesRepository(localDataSource: LocalDataSource,
                           remoteDataSource: RemoteDataSource): TopicRepository {
        return TopicRepository(remoteDataSource, localDataSource)
    }
}