package cl.mauledev.hackernews.di.modules

import android.content.Context
import cl.mauledev.hackernews.data.datasources.local.AppDatabase
import cl.mauledev.hackernews.data.datasources.remote.api.AppService
import cl.mauledev.hackernews.data.datasources.remote.api.NewsAPI
import cl.mauledev.hackernews.di.app.HackerNewsApp
import cl.mauledev.hackernews.utils.Constants
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonDeserializer
import com.google.gson.internal.bind.util.ISO8601Utils
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.text.ParsePosition
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class GeneralModule(var hackerNewsApp: HackerNewsApp) {

    @Provides
    @Singleton
    fun providesApplicationContext(): Context {
        return hackerNewsApp
    }

    @Provides
    @Singleton
    fun providesDatabase(context: Context): AppDatabase {
        return AppDatabase.getAppDatabase(context)
    }

    @Provides
    @Singleton
    fun providesOkHttp(): OkHttpClient {
        return OkHttpClient.Builder()
                .connectTimeout(Constants.REMOTE_CONNECT_TIMEOUT, TimeUnit.MINUTES)
                .readTimeout(Constants.REMOTE_READ_TIMEOUT, TimeUnit.MINUTES)
                .writeTimeout(Constants.REMOTE_WRITE_TIMEOUT, TimeUnit.MINUTES)
                .addInterceptor(HttpLoggingInterceptor(HttpLoggingInterceptor.Logger { message ->
                    Timber.d("OkHTTP: %s", message)
                }))
                .build()
    }

    @Provides
    @Singleton
    fun providesGson(): Gson {

        val deserializer = JsonDeserializer<Date> { json, typeOfT, context ->
            return@JsonDeserializer ISO8601Utils.parse(json.asString,
                    ParsePosition(0))
        }

        val gsonBuilder = GsonBuilder().apply {
            this.setDateFormat(Constants.DATE_FORMAT)
            this.registerTypeAdapter(Date::class.java, deserializer)
        }

        return gsonBuilder.create()
    }

    @Provides
    @Singleton
    fun providesRetrofit(okHttpClient: OkHttpClient, gson: Gson): Retrofit {
        return Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }

    @Provides
    @Singleton
    fun providesRESTService(retrofit: Retrofit): AppService {
        return retrofit.create<AppService>(AppService::class.java)
    }

    @Provides
    @Singleton
    fun providesApi(appService: AppService): NewsAPI {
        return NewsAPI(appService)
    }
}