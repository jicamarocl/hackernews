package cl.mauledev.hackernews.utils

enum class ConnectionState {

    CONNECTED, DISCONNECTED

}