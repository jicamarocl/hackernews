package cl.mauledev.hackernews.utils

import android.arch.persistence.room.TypeConverter
import cl.mauledev.hackernews.data.model.State
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.*

class AppConverters {

    @TypeConverter
    fun fromTimestamp(value: Long?): Date? {
        return if (value == null) null else Date(value)
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return if (date == null) {
            null
        } else {
            date.getTime()
        }
    }

    @TypeConverter
    fun fromState(value: Int): State {
        return State.values()[value]
    }

    @TypeConverter
    fun stateToInt(state: State): Int {
        return state.state
    }

    @TypeConverter
    fun listToString(tags: List<String>?): String? {
        val listType = object : TypeToken<ArrayList<String>>() {}.type
        return Gson().toJson(tags, listType)
    }

    @TypeConverter
    fun stringToList(string: String): List<String>? {
        val listType = object : TypeToken<ArrayList<String>>() {}.getType()
        return Gson().fromJson(string, listType)
    }
}