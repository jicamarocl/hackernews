package cl.mauledev.hackernews.utils

class Constants {

    companion object {

        const val BASE_URL = "https://hn.algolia.com"

        const val REMOTE_CONNECT_TIMEOUT = 1L
        const val REMOTE_READ_TIMEOUT = 1L
        const val REMOTE_WRITE_TIMEOUT = 2L

        const val DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"

        const val DEFAULT_QUERY = "android"

        const val TOPIC_TITLE = "title"
        const val TOPIC_URL = "url"

        const val CONNECTIVITY_ACTION = "cl.mauledev.hackernews.CONNECTIVITY_CHANGE"
    }
}