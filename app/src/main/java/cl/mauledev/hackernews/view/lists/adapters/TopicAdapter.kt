package cl.mauledev.hackernews.view.lists.adapters

import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.util.DiffUtil
import android.view.LayoutInflater
import android.view.ViewGroup
import cl.mauledev.hackernews.R
import cl.mauledev.hackernews.data.model.Topic
import cl.mauledev.hackernews.view.lists.viewholders.TopicViewHolder
import cl.mauledev.hackernews.view.viewmodels.MainViewModel

class TopicAdapter(val viewModel: MainViewModel?) : ListAdapter<Topic, TopicViewHolder>(differCallback) {

    companion object {

        val differCallback = object: DiffUtil.ItemCallback<Topic>() {

            override fun areItemsTheSame(oldItem: Topic?, newItem: Topic?): Boolean {
                return oldItem?.equals(newItem) == true
            }

            override fun areContentsTheSame(oldItem: Topic?, newItem: Topic?): Boolean {
                return oldItem?.hashCode() == newItem?.hashCode()
            }

            override fun getChangePayload(oldItem: Topic?, newItem: Topic?): Any {
                return super.getChangePayload(oldItem, newItem)
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TopicViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.topic_item, parent, false)
        return TopicViewHolder(view, viewType, viewModel)
    }

    override fun onBindViewHolder(holder: TopicViewHolder, position: Int) {
        holder.init(getItem(position))
    }

    override fun onBindViewHolder(holder: TopicViewHolder, position: Int, payloads: MutableList<Any>) {
        super.onBindViewHolder(holder, position, payloads)
    }

    fun getTopic(position: Int?): Topic? {
        position?.let {
            return getItem(position)
        }

        return null
    }
}