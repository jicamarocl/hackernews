package cl.mauledev.hackernews.view.lists.viewholders

import android.support.v7.widget.RecyclerView
import android.text.format.DateUtils
import android.view.View
import android.widget.TextView
import cl.mauledev.hackernews.data.model.Topic
import cl.mauledev.hackernews.view.viewmodels.MainViewModel
import kotlinx.android.synthetic.main.topic_item.view.*
import java.util.*

class TopicViewHolder(itemView: View,
                      viewType: Int,
                      val viewModel: MainViewModel?): RecyclerView.ViewHolder(itemView) {

    val titleView: TextView = itemView.topicTitle
    val statusView: TextView = itemView.topicStatus

    fun init(item: Topic) {
        item.title?.let {
            initTitle(item.title)
        } ?: run {
            initTitle(item.storyTitle)
        }

        initAuthorAndDate(item.author, item.createdAt)
        initAction(item)
    }

    private fun initAction(topic: Topic) {
        itemView.setOnClickListener {
            viewModel?.setSelectedTopic(topic)
        }
    }

    private fun initAuthorAndDate(author: String, createdAt: Date) {
        statusView.text = "$author - ${DateUtils.getRelativeTimeSpanString(createdAt.time)}"
    }

    private fun initTitle(title: String?) {
        titleView.text = title
    }
}