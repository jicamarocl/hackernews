package cl.mauledev.hackernews.view.fragments

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import cl.mauledev.hackernews.R
import cl.mauledev.hackernews.utils.ConnectionUtils
import cl.mauledev.hackernews.utils.Constants
import cl.mauledev.hackernews.utils.SwipeToDeleteCallback
import cl.mauledev.hackernews.view.lists.adapters.TopicAdapter
import cl.mauledev.hackernews.view.viewmodels.MainViewModel
import kotlinx.android.synthetic.main.fragment_topics.*

class TopicsFragment: Fragment() {

    private var viewModel: MainViewModel? = null

    lateinit var topicsAdapter: TopicAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        savedInstanceState?.let {
            initExtras(it)
        } ?: run {
            initExtras(arguments)
        }

        initViewModel()
    }

    private fun initExtras(arguments: Bundle?) {
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(requireActivity()).get(MainViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_topics, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAdapter()
        initSwipeRefresh()
        initRecycler()
        initSelectedTopic()
        initTopics()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val activity = requireActivity() as AppCompatActivity
        val supportActionBar = activity.supportActionBar

        supportActionBar?.setTitle(R.string.app_name)
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
    }

    private fun initSelectedTopic() {
        viewModel?.getSelectedTopic()?.observe(this, Observer {

            if (!ConnectionUtils.isConnected(requireContext()))
                return@Observer

            it?.let {
                val bundle = Bundle().apply {
                    it.title?.let {
                        this.putString(Constants.TOPIC_TITLE, it)
                    } ?: run {
                        this.putString(Constants.TOPIC_TITLE, it.storyTitle)
                    }

                    it.url?.let {
                        this.putString(Constants.TOPIC_URL, it)
                    } ?: run {
                        this.putString(Constants.TOPIC_URL, it.storyUrl)
                    }
                }

                Navigation.findNavController(requireActivity(), R.id.nav_host)
                        .navigate(R.id.topicFragment, bundle)
            }
        })
    }

    private fun initAdapter() {
        topicsAdapter = TopicAdapter(viewModel)
    }

    private fun initSwipeRefresh() {

        viewModel?.checkIsLoading()?.observe(this, Observer {
            it?.let {
                swipeRefresh.isRefreshing = it
            } ?: run {
                swipeRefresh.isRefreshing = false
            }
        })

        swipeRefresh.setOnRefreshListener {
            initTopicsFromRemote()
        }

        swipeRefresh.setColorSchemeColors(ContextCompat.getColor(requireContext(),
                R.color.colorAccent))
    }

    private fun initRecycler() {
        topics.layoutManager = LinearLayoutManager(requireContext(),
                LinearLayoutManager.VERTICAL, false)
        topics.setHasFixedSize(true)
        topics.adapter = topicsAdapter

        val swipeDeleteCallback = object: SwipeToDeleteCallback(requireContext()) {

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder?, direction: Int) {
                val topic = topicsAdapter.getTopic(viewHolder?.adapterPosition)
                topic?.let {
                    viewModel?.deleteTopic(topic.id)
                }
            }

        }

        val itemTouchHelper = ItemTouchHelper(swipeDeleteCallback)
        itemTouchHelper.attachToRecyclerView(topics)
    }

    private fun initTopics() {
        viewModel?.checkTopics()?.observe(this, Observer {
            topicsAdapter.submitList(it)
        })
    }

    override fun onResume() {
        super.onResume()
        initTopicsFromRemote()
    }

    private fun initTopicsFromRemote() {
        viewModel?.getTopics(ConnectionUtils.isConnected(requireContext()))
    }
}