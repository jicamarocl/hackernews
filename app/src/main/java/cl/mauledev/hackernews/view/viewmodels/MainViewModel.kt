package cl.mauledev.hackernews.view.viewmodels

import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.LiveDataReactiveStreams
import android.arch.lifecycle.ViewModel
import cl.mauledev.hackernews.data.model.Topic
import cl.mauledev.hackernews.data.repositories.TopicRepository
import cl.mauledev.hackernews.di.app.HackerNewsApp
import cl.mauledev.hackernews.di.modules.TopicModule
import cl.mauledev.hackernews.utils.ConnectionState
import cl.mauledev.hackernews.utils.SingleLiveEvent
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class MainViewModel: ViewModel(), LifecycleObserver {

    @Inject
    lateinit var repository: TopicRepository

    private var topics: LiveData<List<Topic>>? = null

    private var isLoading: SingleLiveEvent<Boolean> = SingleLiveEvent()

    private var state: SingleLiveEvent<ConnectionState> = SingleLiveEvent()

    private var selectedTopic: SingleLiveEvent<Topic> = SingleLiveEvent()

    init {
        initDagger()
    }

    private fun initDagger() {
        HackerNewsApp.generalComponent.plus(TopicModule(this)).inject(this)
    }

    fun checkTopics(): LiveData<List<Topic>>? {
        if (topics == null) {
            val flowable = repository.getLocalTopics()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())

            topics = LiveDataReactiveStreams.fromPublisher(flowable)
        }

        return topics
    }

    fun checkIsLoading(): SingleLiveEvent<Boolean> {
        return isLoading
    }

    fun checkConnectedState(): SingleLiveEvent<ConnectionState> {
        return state
    }

    fun setConnectedState(stateValue: ConnectionState) {
        state.postValue(stateValue)
    }

    fun getTopics(isConnected: Boolean) {

        if (!isConnected) {
            state.postValue(ConnectionState.DISCONNECTED)
            isLoading.postValue(false)
            return
        }

        state.postValue(ConnectionState.CONNECTED)
        isLoading.postValue(true)

        repository.getTopics()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe({
                    Timber.d("Saved ${it.size} elements")
                    isLoading.postValue(false)
                }, {
                    it.printStackTrace()
                    isLoading.postValue(false)
                }, {
                    Timber.d("Completed!")
                })
    }

    fun deleteTopic(topicId: String) {
        repository.deleteTopic(topicId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Timber.d("Element deleted $it")
                }, {
                    it.printStackTrace()
                }, {
                    Timber.d("Completed!")
                })
    }

    fun getSelectedTopic(): SingleLiveEvent<Topic> {
        return selectedTopic
    }

    fun setSelectedTopic(topic: Topic) {
        selectedTopic.postValue(topic)
    }
}