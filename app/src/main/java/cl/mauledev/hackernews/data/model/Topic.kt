package cl.mauledev.hackernews.data.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import cl.mauledev.hackernews.data.datasources.local.AppContract
import com.google.gson.annotations.SerializedName
import java.util.*


@Entity(tableName = AppContract.topics)
data class Topic(@PrimaryKey @SerializedName("objectID") var id: String,
                 var title: String? = null,
                 var url: String? = null,
                 var author: String = "",
                 var points: Int = 0,
                 @SerializedName("story_text") var storyText: String? = null,
                 @SerializedName("comment_text") var commentText: String? = null,
                 @SerializedName("num_comments") var commentsNumber: Int = 0,
                 @SerializedName("story_id") var storyId: Long? = null,
                 @SerializedName("story_title") var storyTitle: String? = null,
                 @SerializedName("story_url") var storyUrl: String? = null,
                 @SerializedName("parent_id") var parentId: Long? = null,
                 @SerializedName("created_at") var createdAt: Date,
                 var tags: List<String>? = LinkedList(),
                 var state: State = State.SYNCED) {

    @Ignore
    constructor() : this(Random().nextLong().toString(), null, null, "", 0,
            null, null, 0, null, null, null, null,
            Date(), LinkedList(), State.SYNCED)

    override fun equals(other: Any?): Boolean {
        if (other == null)
            return false
        if (other !is Topic)
            return false
        if (other.id != this.id)
            return false

        return true
    }

    override fun hashCode(): Int {
        var hash = 7
        hash = 31 * hash + id.hashCode()
        hash = 31 * hash + state.hashCode()
        return hash
    }
}