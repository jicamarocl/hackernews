package cl.mauledev.hackernews.data.repositories

import cl.mauledev.hackernews.data.datasources.local.LocalDataSource
import cl.mauledev.hackernews.data.datasources.remote.RemoteDataSource
import cl.mauledev.hackernews.data.model.Topic
import io.reactivex.Flowable
import javax.inject.Inject

class TopicRepository @Inject constructor(var remoteDataSource: RemoteDataSource,
                                          var localDataSource: LocalDataSource) {

    fun localIsEmpty(): Boolean {
        return localDataSource.isEmpty()
    }

    fun insertAll(topics: List<Topic>): Flowable<List<Long>> {
        return Flowable.fromCallable {
            localDataSource.insertAll(topics)
        }
    }

    fun getLocalTopics(): Flowable<List<Topic>> {
        return localDataSource.getAll()
    }

    fun getTopics(): Flowable<List<Long>> {
        return remoteDataSource.getNews().map {
            localDataSource.insertAll(it.hits)
        }
    }

    fun deleteTopic(topicId: String): Flowable<Boolean> {
        return Flowable.fromCallable {
            if (topicId.isNotEmpty() && localDataSource.checkTopicExists(topicId)) {
                localDataSource.deleteTopic(topicId)
            } else {
                false
            }
        }
    }
}