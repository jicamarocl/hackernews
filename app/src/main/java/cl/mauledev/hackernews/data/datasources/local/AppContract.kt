package cl.mauledev.hackernews.data.datasources.local

class AppContract {

    companion object {

        const val database = "hackers_news"
        const val topics = "topics"

    }
}