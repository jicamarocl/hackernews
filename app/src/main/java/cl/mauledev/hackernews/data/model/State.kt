package cl.mauledev.hackernews.data.model

enum class State(var state: Int) {

    SYNCED(0), DELETED(1)

}