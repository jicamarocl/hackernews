package cl.mauledev.hackernews.data.datasources.local.daos

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import cl.mauledev.hackernews.data.datasources.local.AppContract
import cl.mauledev.hackernews.data.model.Topic
import io.reactivex.Flowable

@Dao
interface TopicDao {

    @Query("SELECT * FROM " + AppContract.topics + " WHERE state = 0 ORDER BY createdAt DESC")
    fun getAll(): Flowable<List<Topic>>

    @Query("SELECT * FROM " + AppContract.topics + " WHERE state = 1")
    fun getAllDeleted(): Flowable<List<Topic>>

    @Query("SELECT COUNT(*) FROM " + AppContract.topics)
    fun checkTableCount(): Int

    @Query("SELECT COUNT(*) FROM " + AppContract.topics + " WHERE id = :topicId")
    fun checkEntryCount(topicId: String): Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(topics: List<Topic>): List<Long>

    @Query("UPDATE " + AppContract.topics + " SET state = :state WHERE id = :topicId")
    fun updateState(topicId: String, state: Int): Int
}