package cl.mauledev.hackernews.data.model

data class Response(var hits: List<Topic>,
                    var nbHits: Int,
                    var page: Int,
                    var nbPage: Int,
                    var hitsPerPage: Int,
                    var processingTime: Int,
                    var exhaustiveNbHits: Boolean,
                    var query: String,
                    var params: String)