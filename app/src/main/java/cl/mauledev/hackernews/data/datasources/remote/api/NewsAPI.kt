package cl.mauledev.hackernews.data.datasources.remote.api

import cl.mauledev.hackernews.data.model.Response
import io.reactivex.Flowable
import javax.inject.Inject

class NewsAPI @Inject constructor(var api: AppService) {

    fun getNews(): Flowable<Response> {
        return api.getNews()
    }
}