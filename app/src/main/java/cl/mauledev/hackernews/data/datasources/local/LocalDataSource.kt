package cl.mauledev.hackernews.data.datasources.local

import cl.mauledev.hackernews.data.model.State
import cl.mauledev.hackernews.data.model.Topic
import io.reactivex.Flowable
import javax.inject.Inject

class LocalDataSource @Inject constructor(var appDatabase: AppDatabase) {

    fun getAll(): Flowable<List<Topic>> {
        return appDatabase.topicDao().getAll()
    }

    fun insertAll(topics: List<Topic>): List<Long> {
        val items = appDatabase.topicDao().getAllDeleted()

        val results = appDatabase.topicDao().insertAll(topics)

        items.concatMapIterable {
            it.asIterable()
        }.map {
            deleteTopic(it.id)
        }.subscribe()

        return results
    }

    fun deleteTopic(topicId: String, state: State = State.DELETED): Boolean {
        return appDatabase.topicDao().updateState(topicId, state.ordinal) > 0
    }

    fun checkTopicExists(topicId: String): Boolean {
        return appDatabase.topicDao().checkEntryCount(topicId) > 0
    }

    fun isEmpty(): Boolean {
        return appDatabase.topicDao().checkTableCount() > 0
    }
}