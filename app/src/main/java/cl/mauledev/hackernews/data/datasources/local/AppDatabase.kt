package cl.mauledev.hackernews.data.datasources.local

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import android.content.Context
import cl.mauledev.hackernews.data.datasources.local.daos.TopicDao
import cl.mauledev.hackernews.data.model.Topic
import cl.mauledev.hackernews.utils.AppConverters

@Database(entities = [(Topic::class)], version = 1, exportSchema = false)
@TypeConverters(AppConverters::class)
abstract class AppDatabase: RoomDatabase() {

    abstract fun topicDao(): TopicDao

    companion object {

        private var INSTANCE: AppDatabase? = null

        fun getAppDatabase(context: Context): AppDatabase {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(context.applicationContext, AppDatabase::class.java,
                        AppContract.database)
                        .fallbackToDestructiveMigration()
                        .build()
            }

            return INSTANCE as AppDatabase
        }

        fun destroyInstance() {
            INSTANCE = null
        }

    }
}