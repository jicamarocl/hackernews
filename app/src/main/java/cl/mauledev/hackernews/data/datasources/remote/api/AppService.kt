package cl.mauledev.hackernews.data.datasources.remote.api

import cl.mauledev.hackernews.data.model.Response
import cl.mauledev.hackernews.utils.Constants
import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.Query

interface AppService {

    @GET("/api/v1/search_by_date/")
    fun getNews(@Query("query") search: String = Constants.DEFAULT_QUERY): Flowable<Response>
}