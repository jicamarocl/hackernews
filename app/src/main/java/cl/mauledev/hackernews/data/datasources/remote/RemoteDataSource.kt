package cl.mauledev.hackernews.data.datasources.remote

import cl.mauledev.hackernews.data.datasources.remote.api.NewsAPI
import cl.mauledev.hackernews.data.model.Response
import io.reactivex.Flowable
import javax.inject.Inject

class RemoteDataSource @Inject constructor(var api: NewsAPI) {

    fun getNews(): Flowable<Response> {
        return api.getNews()
    }

}