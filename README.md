# HackerNews

This app is submitted for ReignDesign's job application technical test. It's created by me using Kotlin, RxJava2, Android Architecture Components.

Besides, I'm using some other libraries like:

* AppCompat
* Navigation
* Timber
* Retrofit
* OkHTTP
* Gson
* Dagger2

# To run the app

To run the app you'll need the last version of Android Studio and API 27 build tools.
